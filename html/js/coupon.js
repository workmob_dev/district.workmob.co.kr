
fn_ValidationForm = function() {
	if (!$("#cp_type option:selected" ).val()) {
			alert('쿠폰 종류를 선택 해주세요');
			('#cp_type').focus();
			return;
		}

		if (!$("#cp_title" ).val()) {
			alert('쿠폰 제목을 입력 해주세요');
			('#cp_title').focus();
			return;
		}

		// 날짜 체크
		if ($('#cp_startdate').val() && $('#cp_enddate').val()) {
			var date1 = new Date($('#cp_startdate').val());
			var date2 = new Date($('#cp_enddate').val());
			if (date2 - date1 < 0) {
				alert('종료 날짜가 시작날짜 이전일 수 없습니다.');
				return;
			}
		}

		// 쿠폰 위치 확인
		var ary_cprect = $('#cp_rect').val().split(',');
		if (ary_cprect.length != 2) {
			alert('쿠폰번호 위치는 top left 순으로 입력 해주세요');
			$('#cp_rect').focus();
			return;
		}

		var b = false;
		$.each(ary_cprect, function(idx, val) {
			if (!$.isNumeric(val)) {
				alert('위치값은 숫자로만 입력 가능합니다.');
				$('#cp_rect').focus();
				return false;
			}
			b = true;
		});

		if (!b) {
			return;
		}

		// 자동 입력일 경우
		if ($('input[name=cp_auto]').val() == 'Y') {
			if (!$('#cp_count').val() || $('#cp_count').val() < 1) {
				alert('쿠폰 수량을 입력 해주세');
				('#cp_count').focus();
				return;
			}
			if (!$('#cp_length').val() || $('#cp_length').val() < 1) {
				alert('쿠폰 번호 길이를 입력 해주세');
				('#cp_length').focus();
				return;
			}
		}

		// 수동 입력일 경우
		if ($('input[name=cp_auto]').val() == 'N' && !$('#cp_no').val()) {
			alert('쿠폰 번호를 입력 해주세요');
			('#cp_no').focus();
			return;
		}

		//$('#cmd').val('new');
		$('#frmMakeCP').submit();
}

fn_FileUpload = function() {
	$('#fileupload').fileupload({
		type: 'post',
		formData: {
			c: 'upload', cmd: 'up', surveycode: $('#surveycode').val()
		},
		url: 'delegate.mob',
		dataType: 'json',
		add: function(e, data){
			var uploadFile = data.files[0];
			var isValid = true;

			if (!(/png/i).test(uploadFile.name)) {
				alert('png 만 가능합니다');
				isValid = false;
			} else if (uploadFile.size > 5000000) { // 5mb
				alert('파일 용량은 5메가를 초과할 수 없습니다.');
				isValid = false;
			}

			if (isValid) {
				data.submit();
			}
		},
		done: function (e, data) {
			$.each(data.result.files, function (index, file) {
				$("#cpImageView").parent().show();
				$("#cpImageView").prop("src", file.url);
				$("#cp_Image").val(file.name);
			});
		},
		fail: function (e, data) {
			alert('파일 업로드 실패');
		}
	}).click();
}

